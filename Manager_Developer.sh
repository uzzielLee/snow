#!/bin/sh
# bootapp.sh

PGREP="/bin/pgrep -f"

echo " + Manager for Developer. "

case "$1" in

'start')
cd /mnt/mmc/.aloide
node app.js &
cd ~

echo " - Starting ALOIDE." 
echo " - Wait for 30 secounds if you want to use it."
;;

'stop')

pid=$(pidof ALOOH_UpnpDEVICE)
if [ $pid ]; then
 kill -9 $(pidof ALOOH_UpnpDEVICE)
wait
fi

pid=$(${PGREP} "node app.js")
if [ $pid ]; then
kill -9 $pid
wait
fi

echo " - Stopped ALOIDE. "
;;
  

*)
echo "usage: $0 {start|stop}"
esac

# EndOfFile
