#!/bin/sh

echo "init.sh"

sysctl -w kernel.printk="2 4 1 7"

#export HOME=/root/snow
export HOME=/mnt/mmc/snow
 
$HOME/.script/util/RGBLED 6

if [ ! -d "$HOME" ]; then

# Will enter here if $DIRECTORY doesn't exist
# LED ON - 
$HOME/.script/util/RGBLED 2

else

cd $HOME

source $HOME/runenv.sh

 if [ "$HOME" = "/mnt/mmc/snow" ]; then
   rm -rf /usr
   ln -s /mnt/mmc/usr/ /usr
   swapon -a
 else
  
  $HOME/.script/util/RGBLED 5 
  node /root/snow/.script/init/ledoff  & 

 fi
   
fi # home_dir check 

# System Time 

export TZ=KST-9
hwclock -s 

