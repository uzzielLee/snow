var readline = require('readline');
var fs = require('fs');
var exec = require('child_process').exec;
//var ETH0_IP=0;
//var ETH0_NETMASK=1;
//var ETH0_GATEWAY=2;
//var ETH0_DNS1=3;
//var ETH0_DNS2=4;
var WLAN0_IP=0;
var WLAN0_NETMASK=1;
var WLAN0_GATEWAY=2;
var WLAN0_DNS1=3;
var WLAN0_DNS2=4;
var WLAN0_ENC=5;
var WLAN0_SSID=6;
var WLAN0_PSK=7;
var DONE = 8;

var WEP_OFFSET=2;
//var INTERFACE_SKIP_OFFSET=5;
var INTERFACE_SKIP_OFFSET=0;

var title_msgs_array = [
  //"* Enter Ethernet IP Address : ", 
  //"* Enter Ethernet NetMask : ", 
  //"* Enter Ethernet Gateway : ", 
  //"* Enter Ethernet Primary DNS  : ",
  //"* Enter Ethernet Sencondary DNS  : ",
  "* Enter wlan IP Address  : ",
  "* Enter wlan NetMask  : " ,
  "* Enter wlan Gateway : ",
  "* Enter wlan primary DNS  : ",
  "* Enter wlan sencondary DNS  : ",
  "* Select Encryption (wpa/wep/open): ",
  "* Enter SSID  : ",	
  "* Enter PSK  : "
];

var body_prefix_array = [
  //"\naddress ",
  //"\nnetmask ",
  //"\ngateway ",
  //"\ndns-nameservers ",
  //", ",
  "\naddress ",
  "\nnetmask ",
  "\ngateway ",
  "\ndns-nameservers ",
  ", ",
  "",
  "\nwpa-ssid ",
  "\nwpa-psk ",
  "\nwireless-essid ",
  "\nwireless-key ",
  
];
var CFG_PATH = process.env.CFGPATH;
var INTERFACE_FILE_PATH = process.env.NPATH;

var title_ip="\naddress";
var title_netmask="\nnetmask";
var title_gateway="\ngateway";
var title_dns="\ndns-nameservers";
var wlan0_ssid;
var interfaces = 'auto lo\niface lo inet loopback';
var ethernet_category_msg = "\n\nauto eth0\niface eth0 inet static";
var wlan_category_msg = "\n\nauto wlan0\niface wlan0 inet static";
var title_msg;
var select_enc_type;
var body_prefix;


var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function doCommandLine(seperate)
{
  var valid;	
  title = title_msgs_array[seperate];
  
  
  rl.question(title, function(data) {
/*    
	if(seperate == ETH0_IP)
	{	
		if(data == 'skip')
		{
			console.log('Skipped Ethernet !!!');
			doCommandLine(seperate+INTERFACE_SKIP_OFFSET);
			return;
		}
		else
		{
			interfaces += ethernet_category_msg;
		}
	}
	*/
	if(seperate == WLAN0_IP)
	{
		if(data == 'skip')
		{
			console.log('Skipped Wlan !!!');
			writeInterface(interfaces);
			return;
		}
		else
		{
			interfaces += wlan_category_msg;
		}
	}
	
 
	if(seperate != WLAN0_ENC && seperate != WLAN0_SSID && seperate != WLAN0_PSK)
	{
		if(checkIP(data) == false)//?�효??체크
		{
			console.log('Invalid Input!!!');
			doCommandLine(seperate);
			return;
		}
	}

		
    if(seperate == WLAN0_ENC)
	{
		if(data == "wpa" || data == "wep" || data == "open")
		{
			select_enc_type = data;
			doCommandLine(++seperate);
		}
		else
		{
			console.log('Invalid Input!!!');
			doCommandLine(seperate);
		}
	}
    else if(seperate == WLAN0_PSK)
    {
	  if(select_enc_type == "wep")
	  {
		interfaces += body_prefix_array[seperate+WEP_OFFSET]+data;
		console.log('\n\n'+ interfaces);
		
		  generateWpaConf(select_enc_type, data);
	  
			}
	  else//wpa
			{
	    generateWpaConf(select_enc_type, data);
		}//wpa
    }
    else
    {
	  if(seperate == WLAN0_SSID && ( select_enc_type == "wep" || select_enc_type == "open"))
		interfaces += body_prefix_array[seperate+WEP_OFFSET]+data;
	  else
		interfaces += body_prefix_array[seperate]+data;
		
	  if(seperate == WLAN0_SSID)
        wlan0_ssid = data;

	if(select_enc_type == "open")
	{
		 generateWpaConf(select_enc_type, data);
	}
	else
      doCommandLine(++seperate)
    }
  });
};

function checkIP(strIP) {
    var expUrl = /^(1|2)?\d?\d([.](1|2)?\d?\d){3}$/;
    return expUrl.test(strIP);
}

function writeInterface(data){
	fs.writeFile(CFG_PATH+'/interfaces', data, function(err){
	var query = 'cp '+CFG_PATH+'/interfaces /etc/network/interfaces';
	if(err == null)
	{
	  child  = exec(query,
	  function(error, stdout, stderr) {
		if(error == null)
		{
			fs.writeFile(CFG_PATH+"/network.conf", 'static' , function(err){
			var query = 'sync';
			if(err == null)
			{
				child  = exec(query,
				function(error, stdout, stderr) {
					if(error == null)
					{
						console.log('Complete');
						process.exit();
					}
			  });
			 }
			else
			  console.log(err);
		});
		}
	  });
	  
	 }
	else
	  console.log(err);
});

}



function generateWpaConf(encType, psk)
{
  
  if(encType == "wpa")
  {
  		  var child;
		  rl.close();
		  child = exec('wpa_passphrase '+wlan0_ssid+' '+psk+' > '+CFG_PATH+'/.wpa2.conf',
		
		  function(error, stdout, stderr) {
	  
			if (error != null) {
			  console.log('exec error: ' + error);
			}
			else
			{
			  child = exec('grep psk '+CFG_PATH+'/.wpa2.conf| grep -Ev \'#\' | awk -F = \'{print $2}\'',
				  function(error, stdout, stderr) {
				  
				  interfaces += body_prefix_array[WLAN0_PSK]+stdout;
				  console.log('\n\n'+ interfaces);
				  writeInterface(interfaces);
			   });
			  }
			});
		
  }
  else
  {
    
  
  var data1 = "ctrl_interface=/var/run/wpa_supplicant\n\
\n\
network={\n\
ssid=" + "\"" + wlan0_ssid + "\"\n";


data1 +="key_mgmt=NONE\n";

if(encType == "wep")
{
  data1 +="wep_key0"+"=\""+psk + "\"\n";
}


data1 +="}";


fs.writeFile(CFG_PATH+'/.wpa2.conf', data1, function(err) {
		fs.writeFile(CFG_PATH+'/network.conf', 'static' , function(err){
			var query = 'sync';
			if(err == null)
			{
				child  = exec(query,
				function(error, stdout, stderr) {
					if(error == null)
					{
					  writeInterface(interfaces);
					}
			  });
			 }
			else
			  console.log(err);
	});
});
}
}





doCommandLine(WLAN0_IP);
