#!/bin/sh

 ifdown -f wlan0
 ifconfig wlan0 down
# Stop wps_network


# Stop if there's run_network_eth for udhcpc trying to connect

ps | grep Manager_Network | grep -v grep | grep -v "$PPID" | awk '{print $1}' | xargs kill -9 > /dev/null

ftpTrigger=$(ps | grep -v grep | grep ftp_trigger | awk '{print $1}')
if [ -n $ftpTrigger ] ; then
kill -9 $ftpTrigger > /dev/null
fi

pid=$(pidof udhcpc)
if [ $pid ] ; then 
 kill -9 $(pidof udhcpc)
 wpa_cli -iwlan0 -p/var/run/wpa_supplicant remove_network 0
 wait
fi

pid=$(pidof wpa_supplicant)
if [ $pid ]; then
 kill -9 $(pidof wpa_supplicant)
 wait
fi

# $NPATH/netled.sh stop
 
# Stop SoftAP Network

pid=$(pidof udhcpd)
if [ $pid ]; then
 kill -9 $(pidof udhcpd)
 wait
fi

pid=$(pidof hostapd)
if [ $pid ]; then
 kill -9 $(pidof hostapd)
 wait
fi 

# Common (Clinet / AP)
pid=$(pidof ALOOH_UpnpDEVICE)
if [ $pid ]; then
 kill -9 $(pidof ALOOH_UpnpDEVICE)
 wait
fi 

# kill node file uploader.js

pid=$(pidof node)
if [ $pid ]; then 
kill -9 $(pidof node)
fi 
  
 ifdown -f eth0
ifconfig eth0 down

