#!/bin/sh

ifconfig wlan0 0.0.0.0 up

if [ -f $HOME/.script/macaddr ]; then
MAC=$(cat /mnt/mmc/snow/.script/macaddr)
else
MAC=AA:AA:AA:AA:AA:FF
fi

ifconfig eth0 hw ether $MAC
ifconfig eth0 up

while [ -z "$IP_Check" ]
do
udhcpc -i eth0 -s /usr/share/udhcpc/default.script -nq -t 3 > /dev/null 
sleep 10 
IP_Check=$(ip addr show eth0 | awk '/inet / {print $2}' | cut -d/ -f 1)
done
exit 0
