var readline = require('readline');
var fs = require('fs');
var exec = require('child_process').exec;



var title_msgs_array = [
  "* Select Encryption (wpa/wep/open): ",
  "* Enter SSID  : ",	
  "* Enter PSK  : "
];

var WLAN0_ENC=0;
var WLAN0_SSID=1;
var WLAN0_PSK=2;

var INTERFACE_FILE_PATH ;

var CFG_PATH = process.env.CFGPATH;


var wlan0_ssid;
var select_enc_type;
var wlan0_psk;

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


function doCommandLine(seperate)
{
	title = title_msgs_array[seperate];
	
	rl.question(title, function(data) {
		if(seperate == WLAN0_ENC)
		{
			if(data == "wpa" || data == "wep" || data == "open")
			{
				select_enc_type = data;
				doCommandLine(++seperate);
			}
			else
			{
				console.log('Invalid Input!!!');
				doCommandLine(seperate);
			}
		}
		else if(seperate == WLAN0_SSID)
		{
			wlan0_ssid=data;
                        if(select_enc_type == "open")
                           doWEP_N_OPEN(data);
                        else
			   doCommandLine(++seperate);
		}
		else
		{
			if(select_enc_type == "wpa")
				doWPA(data);
			else
				doWEP_N_OPEN(data);
		}
	});
	
};


function doWPA(psk)
{
	child = exec('wpa_passphrase '+wlan0_ssid+' '+psk+' > '+CFG_PATH+'/.wpa2.conf',
		
		  function(error, stdout, stderr) {
	  
			if (error != null) {
			  console.log('exec error: ' + error);
			}
			else
			{
				fs.writeFile(CFG_PATH+'/network.conf', 'dhcp' , function(err){

							if(err == null)
							{
								child  = exec('sync',
								function(error, stdout, stderr) {
									if(error == null)
									{
										console.log('Complete');
										process.exit();
									}
							  });
							 }
							else
							  console.log(err);
				});
			}
		});
}

function doWEP_N_OPEN(psk)
{
  var data1 = "ctrl_interface=/var/run/wpa_supplicant\n\
\n\
network={\n\
ssid=" + "\"" + wlan0_ssid + "\"\n";


data1 +="key_mgmt=NONE\n";

if(select_enc_type == "wep")
data1 +="wep_key0"+"=\""+psk + "\"\n";



data1 +="}";




fs.writeFile(CFG_PATH+"/.wpa2.conf", data1, function(err) {
		fs.writeFile(CFG_PATH+"/network.conf", 'dhcp' , function(err){
			var query = 'sync';
			if(err == null)
			{
				child  = exec(query,
				function(error, stdout, stderr) {
					if(error == null)
					{
						console.log('Complete');
						process.exit();
					}
			  });
			 }
			else
			  console.log(err);
	});
});
}
doCommandLine(WLAN0_ENC);			 
