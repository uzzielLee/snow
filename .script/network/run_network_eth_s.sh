#!/bin/sh

if [ -f $HOME/.script/macaddr ]; then
MAC=$(cat /mnt/mmc/snow/.script/macaddr)
else
MAC=AA:AA:AA:AA:AA:AA
fi 

#echo $MAC
ifconfig eth0 hw ether $MAC
wait

#ethernet=$(cat /etc/network/interfaces | grep eth0 | grep -v '#')
ethernet=$(cat /etc/network/interfaces | grep eth0)

if [ -n "$ethernet" ] ; then

ipCnt=$(grep -c address /etc/network/interfaces)

if [ "$ipCnt" = "2" ] ; then
	ifup eth0
elif [ "$ipCnt" = "1" ] ; then

	wifiInfo=$(grep wpa-ssid /etc/network/interfaces)
	if [ -z "$wifiInfo" ] ; then
		ifup eth0
	else
		echo "No Setting Data for Ethernet" 
	fi
	
else #No address
echo "No Setting Data for Ethernet" 
fi   #No address
else #No "eth0"
echo "No Setting Data for Ethernet" 
fi   #No "eth0"
