#!/bin/sh

echo "======= run_softap_mode.sh ~~~~~~"

# Set IP Address
ifconfig wlan0 192.168.21.1 up
wait

# Run DHCP Demon
/sbin/udhcpd
wait

# Run Soft Access Point
$HOME/.script/softap/hostapd /root/rtl.conf -B
wait
