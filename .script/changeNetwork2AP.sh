#!/bin/sh

# wlan0 down
ifconfig wlan0 down

# Stop wps_network
kill -9 $(pidof udhcpc)
#wait
sleep 1

kill -9 $(pidof wpa_supplicant)
wait

# Stop SoftAP Network
kill -9 $(pidof udhcpd)
wait
kill -9 $(pidof hostapd)
wait

# kill node file uploader.js
kill -9 $(pidof node)
wait
