#!/bin/sh

IP=$(ip addr show wlan0 | awk '/inet/ {print $2}' | cut -d/ -f 1)
MAC=$(ip link show wlan0 | awk '/ether/ {print $2}')
#NAME=$(sed -e 's/"/ /g' -e 's/{/ /g' -e 's/}/ /g' -e 's/:/ /g' -e 's/,/ /g' /root/snow/.config.db | awk '{printf $4}')
NAME=$($HOME/.script/init/serial_only)
echo $NAME

if [ -z "$NAME" ] ;
then
NAME=null;
fi
/mnt/mmc/.aloide/upnp_device/ALOOH_UpnpDEVICE $IP 9001 $MAC $NAME

