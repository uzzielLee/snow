#!/bin/sh

kill -9 $(pidof node)

echo "22" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio22/direction

GPIOVALUE=$(cat /sys/class/gpio/gpio22/value)
gp="1"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
MAC=$(ip link show wlan0 | awk '/ether/ {print $2}')
IP=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
# wlan0 down
ifconfig wlan0 down
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  wlan0 down  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
# Stop wps_network
kill -9 $(pidof udhcpc)
wpa_cli -iwlan0 -p/var/run/wpa_supplicant remove_network 0
wait
kill -9 $(pidof wpa_supplicant)
wait
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  3  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
# Stop SoftAP Network
kill -9 $(pidof udhcpd)
wait
kill -9 $(pidof hostapd)
wait

#echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  4  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
# kill node file uploader.js
#kill -9 $(pidof node)
#echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  5  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
#sleep 3 
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  6  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

ifconfig wlan0 192.168.21.1 up
wait 

/sbin/udhcpd
wait

$HOME/.script/softap/hostapd $HOME/.script/softap/rtl_hostapd.conf -B
wait

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  7  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
node $HOME/.script/init/fileuploader.js & 
wait
source $HOME/.script/run_wifi.sh
#
