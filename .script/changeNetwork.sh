#!/bin/sh

# wlan0 down
ifconfig wlan0 down

# Stop wps_network
kill -9 $(pidof udhcpc)
wpa_cli -iwlan0 -p/var/run/wpa_supplicant remove_network 0
wait
kill -9 $(pidof wpa_supplicant)
wait

# Stop SoftAP Network
kill -9 $(pidof udhcpd)
wait
kill -9 $(pidof hostapd)
wait
kill -9 $(pidof ALOOH_UpnpDEVICE)
wait

# kill node file uploader.js
kill -9 $(pidof node)
wait
