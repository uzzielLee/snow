var fs = require('fs');
var express = require('express');
var snowlib = require('snow/snowlib');
var GPIO = require('snow/gpio');
var http = require('http');
var sys = require('sys')
var exec = require('child_process').exec;
var led = require('snow/led');
var nGpio = 9;
function puts(error, stdout, stderr) { sys.puts(stdout) }


var auid = snowlib.readAuid();
var serial = snowlib.readSerial();

console.log(auid);

var key;
var new_key;
var name;
var new_name;
var ip;
var email;
var password;
var akey;
var topicid; 
var url; 
var intervalHandler;

var index=0;
var count=0;
var CFG_PATH;
var SERVER_IP = process.env.SERVER_URL;
var SERVER_PORT = process.env.SERVER_PORT;
var CFG_PATH = process.env.CFGPATH;
var os=require('os');
var ifaces=os.networkInterfaces();

CheckSerial();
function CheckSerial() {

    var data = {
        'data': {
            'serial': serial.toString("utf8",0,12),
        }
    };

    var dataString = JSON.stringify(data);

    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
    };

    var options = {
        host: SERVER_IP,
        port: SERVER_PORT,
        path: '/api/v1/devices/serial',
        method: 'POST',
        headers: headers
    };

   
   var req = http.request(options, function(response)
   {
      var str ='';
      response.on('data', function (chunk) {
        str += chunk;
        
       
      });
    
    response.on('end', function () {

    var checkResult = JSON.parse(str);

    fs.readFile(CFG_PATH+"/.config.db", { encoding: 'utf8' }, function (err, config) {
      var dString = JSON.parse(config);
  
      key = dString.key; 
      name = dString.name;
      new_key = dString.new_key;
      new_name = dString.new_name;
  
      for (var dev in ifaces) {
        var alias=0;
        ifaces[dev].forEach(function(details){
          if (details.family=='IPv4') {
            console.log(dev+(alias?':'+alias:''),details.address);
            console.log('+++++++++++++++++ip : '+details.address);
            if (dev =="wlan0") {
              ip = details.address; 
              console.log('ipAddress: '+ip);
              if(checkResult.data.result == 'Not registered')
                Create();
              else if(checkResult.data.result == 'Registered')
                Auid();  
              else
                console.log("Serial Error");
            }  
            ++alias;
          }
        });
      }   
      if(ip == null || ip == "127.0.0.1")
      {
        process.exit();
      }
            console.log('@@@@@@@@@@@@@@@@@@  ip: ' + ip);
    });
});
   });

    req.write(dataString);

    req.on('error', function (e) {
        led.blink(3, 1000);
        console.log("Got error: " + e.message);
    });
    req.end();

    console.log(options);
};





callback = function (response) {
    var str = ''
    response.on('data', function (chunk) {
        str += chunk;
    });

    response.on('end', function () {
        console.log('testa'+str);

        
        if (str.indexOf("error") == -1) {
            var dStr = JSON.parse(str);

            if(new_key)
            {
              fs.writeFile(CFG_PATH+"/.config.db", "{\"key\":\"" + new_key + "\",\"name\":\"" + new_name+"\"}", function(err) {
                if (dStr.data.key) {
                  console.log('has key');
                key = dStr.data.key;
                Auid();
                }
		led.on(5);
              });
            }
            else
            {
            
              fs.writeFile(CFG_PATH+"/.config.db", "{\"key\":\"" + key + "\",\"name\":\"" + new_name+"\"}", function(err) {
                console.log(dStr.data.key);
                if (dStr.data.key) {
                key = dStr.data.key;
                Auid();
                }
                led.on(5);
                });
            }
        }
        else
        {
          fs.writeFile(CFG_PATH+"/.config.db", "{\"key\":\"" + key + "\",\"name\":\"" + name+"\"}", function(err) {
          var eStr = JSON.parse(str);
          if(eStr.error == "Auid not found")
          {
            console.log('1 eStr.error :'+eStr.error);  
            led.on(1)
          }
          else if(eStr.error == "Device not found")
          {
            console.log('2 eStr.error :'+eStr.error);
            led.blinkEx(1, 1);
          }
          else if(eStr.error == "Unauthorized")
          {
            console.log('3 eStr.error :'+eStr.error);
            led.blinkEx(1, 2);
          }
          else if(eStr.error == "Internal Server Error")
          {
            console.log('4 eStr.error :'+eStr.error);
            led.blinkEx(1, 3);                        
          }
          else
          {
            console.log('the others Error');
          }
          });
          //led.blink(6, 1000);
        }
    });
};
callback1 = function (response) {

    var str = ''
    response.on('data', function (chunk) {
        str += chunk;
    });

    response.on('end', function () {

        console.log('trst1'+str);

        if (str.indexOf("error") == -1) {
            var dStr = JSON.parse(str);
            //var strinItems = JSON.stringify(dStr.data.keys);
            var strinItems = dStr.data.keys;

            console.log(dStr.data.keys);

            strinItems = strinItems.filter(function (item) {
                //return (item.permission == "2");
                console.log(item.label);

                if (item.label == "Basic Key") {
                    fs.readFile(CFG_PATH+'/.updated', { encoding: 'utf8'}, function(err, data){
                    
                    
                    Update(item.key, data);
                    });
                }
            })
        }
        else {
          var eStr = JSON.parse(str);
          
          if(eStr.error == "Unauthorized")
          {
            console.log('eStr.error :'+eStr.error);
            led.blinkEx(1, 2);
          }
          else
          {
            Create(auid, key, name);
          }
        }

    });
}

function Auid() {

    var data = {
        'data': {
            'auid': auid.toString("utf8",0,36),
            'type': "3"// all 
        }
    };

    var dataString = JSON.stringify(data);

    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
        'Authorization': "Bearer " + key
    };

    var options = {
        host: SERVER_IP,
        port: SERVER_PORT,
        path: '/api/v1/devices/auid',
        method: 'POST',
        headers: headers
    };

    console.log('test2'+dataString);

   var req = http.request(options, callback1);

    req.write(dataString);

    req.on('error', function (e) {
        led.blink(3, 1000);
        console.log("Got error: " + e.message);
    });
    req.end();

    console.log(options);
};

function Update(akey, updatedkey){

    var data = {
        'data': {
            'name': new_name,
            'status': "OFF",            // "OFF"
            'ip': ip, // all 
            'checkUpdate' : updatedkey,
        }
    };

    var dataNetChg = {
        'data': {
            'status': "SET",            // "OFF"
            'ip': ip // all 
        }
    };
    
/*    var data = {
        'data': {
            'name': name,
            'status': "ON",            // "OFF"
            'ip': ip // all 
        }
    }; */

    var dataString;
    
    if(new_name == null)
      dataString = JSON.stringify(dataNetChg);  
    else
      dataString = JSON.stringify(data);            
    
    

    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': dataString.length,
        'Authorization': "Bearer " + akey
    };

    var options = {
	      host: SERVER_IP,
        port: SERVER_PORT,
        path: '/api/v1/devices/update',
        method: 'POST',
        headers: headers
    };

    console.log(dataString);

    var req = http.request(options, callback);
    // req.write(dataString);
    req.end(dataString);
    req.on('error', function (e) {
        led.blink(3, 1000);
        console.log("Update got error: " + e.message);
    });
    console.log(options);

};

function Create() {

    var data = {
        'data': {
            'auid': auid.toString("utf8",0,36),
            'name': new_name
        }
    };

    var dataString = JSON.stringify(data);
    var headers;
    if(new_key)
    {
      headers = {
          'Content-Type': 'application/json',
         'Content-Length': dataString.length,
          'Authorization': "Bearer " + new_key
      };
    }
    else
    {
      headers = {
          'Content-Type': 'application/json',
         'Content-Length': dataString.length,
          'Authorization': "Bearer " + key
      };
      
    }
    //http://api.alooh.io/api/v1/devices/register
    var options = {
	      host: SERVER_IP,
        port: SERVER_PORT,
        path: '/api/v1/devices/register',
        method: 'POST',
        headers: headers
    };

    console.log(dataString);

    var req = http.request(options, callback);
    // req.write(dataString);
    req.end(dataString);
    
    req.on('error', function (e) {
        led.blink(3, 1000);
        console.log("Create got error: " + e.message);
    });

    console.log(options);
};
