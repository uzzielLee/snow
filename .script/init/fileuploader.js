var fs = require('fs');
var sys = require('sys'); 
var express = require('express');
var morgan = require('morgan'); 
var bodyParser = require('body-parser'); 
var methodOverride = require('method-override');
var exec = require('child_process').exec;
var snowlib = require('snow/snowlib');
var led = require('snow/led');
var app = express();
path = require('path');
ejs = require('ejs-locals');

var auid = snowlib.readAuid(); 
var serial = snowlib.readSerial();
app.use(morgan('dev'));
app.use(bodyParser());
app.use(methodOverride());

app.engine('html', ejs);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.use(express.static(path.join(__dirname, 'public')));
var status = 0;
var intervalHandler;
var index = 0;
var count = 0;
var existedName  = null;
var CFG_PATH = process.env.CFGPATH;
var HOME_PATH = process.env.HOME;

led.on(4);
/*app.configure(function() {
  app.use(express.bodyParser());
});
*/

app.post('/upload', function(req, res) {

  fs.readFile(req.files.file.path, function(error, data) {
    var fileName = req.files.file.name;

    if (!fileName) {
      res.end();
    } else {
      var Path = __dirname + "/" + fileName;

      fs.writeFile(Path, data, function(err) {
        res.writeHead(200, {
          'Content-Type': 'text/html'
        });
        res.end('Upload success');
      });
    }
  });
});

app.post('/close', function(req, res) {
  app.close();
});

app.post('/data', function(req, res) {

  var body = '';
  var data_fix;
  
  if(!req.body.wepkeytype && req.body.passwd)
  {
    var query_wpa = 'wpa_passphrase '+req.body.ssid+' '+req.body.passwd+' > '+CFG_PATH+'/.wpa2.conf';
    console.log(query_wpa);
    
    var child_wpa;
    child_wpa = exec(query_wpa, function(error, stdout, stderr) {
                  if(error == null)
                  {
                    var qurey=null;
                    if(existedName)
                    {
                      query = "\"new_name\":\"" + req.body.name + "\", \"name\":\""+existedName+"\"}";
                    }
                    else
                    {
                      query = "\"new_name\":\"" + req.body.name + "\"}";
                    }
                    fs.appendFile(CFG_PATH+"/.config.db", query, function(err) {
                      if (err) {
                        console.log(err);
                      } else {
                        console.log("The file was saved!");
                        
                         fs.writeFile(CFG_PATH+'/network.conf', 'dhcp', function(err){
                          var child;
                          child = exec(HOME_PATH+'/.script/changeNetwork.sh',
                        
                          function(error, stdout, stderr) {
            
                            if (error != null) {
                              console.log('exec error: ' + error);
                            }
                            else
                            {
                            	
                            	console.log(CFG_PATH+' netowork file write');
                            	  {
                            	    exec('sync', function(error, stdout, stderr){
                            	    if(error == null)
                            	    console.log('Complete');
                            	    else
                            	    console.log(error);
                            	process.exit();
                            	});
	                    }
	                    }
                          });
                         });
                      }
                    });
                  }
                });
  }
else
{ 
  var data1 = "ctrl_interface=/var/run/wpa_supplicant\n\
\n\
network={\n\
ssid=" + "\"" + req.body.ssid + "\"\n";
if(req.body.proto)
{
  data1 +="proto="+req.body.proto+"\n";
}
if(req.body.key_mgmt)
{
  data1 +="key_mgmt="+req.body.key_mgmt+"\n";
}
if(req.body.pairwise)
{
  data1 += "pairwise="+req.body.pairwise+"\n";
}
if(req.body.group)
{
  data1 +="group="+req.body.group+"\n";
}

if(req.body.wepkeytype)
{
  data1 +=req.body.wepkeytype+"=\""+req.body.passwd + "\"\n";
}
else
{
  if(req.body.passwd)
  {
    data1 +="psk=\"" + req.body.passwd + "\"\n";
  }
}

data1 +="}";

var data2 = "ctrl_interface=/var/run/wpa_supplicant\n\
\n\
network={\n\
ssid=" + "\"" + req.body.ssid + "\"\n\
psk=\"" + req.body.passwd + "\"\n\
}";
//  if(req.body.proto == null)
//    data_fix = data2;
//  else
    data_fix = data1;
    
  fs.writeFile(CFG_PATH+"/.wpa2.conf", data_fix, function(err) {
    if (err) {
      console.log(err);
    } else {
      console.log("The file was saved!");
      
        var qurey=null;
        if(existedName)
        {
          query = "\"new_name\":\"" + req.body.name + "\", \"name\":\""+existedName+"\"}";
        }
        else
        {
          query = "\"new_name\":\"" + req.body.name + "\"}";
        }
        fs.appendFile(CFG_PATH+"/.config.db", query, function(err) {
          if (err) {
            console.log(err);
          } else {
            console.log("The file was saved!");
            
             fs.writeFile(CFG_PATH+'/network.conf', 'dhcp', function(err){
              var child;
              child = exec(HOME_PATH+'/.script/changeNetwork.sh',
            
              function(error, stdout, stderr) {

                if (error !== null) {
                  console.log('exec error: ' + error);
                }
              });
              });
          }
        });
    }
  });
  }//if(!req.body.wepkeytype && req.body.passwd)
  res.writeHead(200, "OK", {
  'Content-Type': 'text/html',
	'Auid': auid.toString('utf8',0,36)
});



res.end('test1234');
});


function writeInterface(data){

	fs.writeFile(CFG_PATH+'/interfaces', data, function(err){
	var query = 'cp '+CFG_PATH+'/interfaces /etc/network/interfaces';
	if(err == null)
	{
	  child  = exec(query,
	  function(error, stdout, stderr) {
		if(error == null)
		{
			fs.writeFile(CFG_PATH+'/network.conf', 'static' , function(err){

			if(err == null)
			{
				child = exec('sync', function(error, stdout, stderr){
				});
				console.log('Complete');
			}
			else
				console.log(err);
		});
		}
	  });
	  
	 }
	else
	  console.log(err);
});

}

function writeNetworkConf(){
	fs.writeFile(CFG_PATH+'/network.conf', 'dhcp' , function(err){

		if(err == null)
		{
			child = exec('sync', function(error, stdout, stderr){
			});
			console.log('Complete');
		}
		else
			console.log(err);
	});
}

app.post('/network', function(req, res) {

  var body = '';

  var data1 = '';
  var staticIfaces = "auto lo\niface lo inet loopback\n\nauto wlan0\niface wlan0 inet static";
    //Static IP Address
	if(req.body.ipaddress != null)
	{
	  
	  
	  staticIfaces += "\naddress "+req.body.ipaddress;
  	staticIfaces += "\nnetmask "+req.body.subnetmask;
  	staticIfaces += "\ngateway "+req.body.gateway;
  	staticIfaces += "\ndns-nameservers "+req.body.dns1+", "+req.body.dns2;
  	if(req.body.authtype == "WPA/WPA2")
  	{
  		staticIfaces += "\nwpa-ssid "+req.body.ssid;
  //		interfaces += "\nwpa-psk "+req.body.psk;
  	}
  	else if(req.body.authtype == "WEP")
  	{
  		staticIfaces += "\nwireless-essid "+req.body.ssid;
  		staticIfaces += "\nwireless-key "+req.body.passwd;
  	}
  	else if(req.body.authtype == "OPEN")
  	{
  		staticIfaces += "\nwireless-essid "+req.body.ssid;
  	}
	  	
	}	
  	
  
  if(req.body.authtype === "WPA/WPA2"){
  		var query_wpa = 'wpa_passphrase '+req.body.ssid+' '+req.body.passwd+' > '+CFG_PATH+'/.wpa2.conf';
    
    console.log(query_wpa);
    
    var child_wpa;
    child_wpa = exec(query_wpa, function(error, stdout, stderr) {
      if(error == null)
      {
  				console.log("The file was saved!");
  	      
  	      if(req.body.ipaddress != null)//static IP Setting
  				{
  				  child = exec('grep psk $CFGPATH/.wpa2.conf | grep -Ev \'#\' | awk -F = \'{print $2}\'',
  				  function(error, stdout, stderr) {
  				  
    				  	staticIfaces += "\nwpa-psk "+stdout;
              				 	writeInterface(staticIfaces);						
              				 	res.render('wifisetup_ip_result');
      			      		});
				 }
				 else
				 {	
              					writeNetworkConf();
         res.render('wifisetup_result');
				 }
      }
  			else
  				console.log(error);
    });
  }
  else
  {
  if(req.body.authtype === "WEP"){
    data1 = "ctrl_interface=/var/run/wpa_supplicant\n\
\n\
network={\n\
ssid=" + "\"" + req.body.ssid + "\"\n\
wep_key0=\"" + req.body.passwd + "\"\n\
key_mgmt=NONE\n\
}";
  }
  else if(req.body.authtype === "OPEN"){
    data1 = "ctrl_interface=/var/run/wpa_supplicant\n\
\n\
network={\n\
ssid=" + "\"" + req.body.ssid + "\"\n\
key_mgmt=NONE\n\
}";
  }


  fs.writeFile(CFG_PATH+"/.wpa2.conf", data1, function(err) {
    if (err) {
      console.log(err);
    } else {
      console.log("The file was saved!");
		  if(req.body.ipaddress != null)//static IP Setting
		  {
		    writeInterface(staticIfaces);	
		    res.render('wifisetup_ip_result');
		  }
		  else
		  {
		    writeNetworkConf();
		    res.render('wifisetup_result');  
		  }
    }
  });

  }
  
  if(req.body.ipaddress != null)      
  {
  }
});

app.listen(9001, function() {
  console.log("AP Mode Server Start!");
})


app.get('/', function(req, res) {
  res.render('wifisetup');
})

app.get('/wifisetup', function(req, res) {
  res.render('wifisetup');
  })
app.get('/wifisetup_ip', function(req, res) {
  res.render('wifisetup_ip');
  })

app.post('/key', function(req, res) {
	
  if(req.body.serial == null || req.body.serial != serial)
  {
    res.writeHead(900, {"Content-Type": "text/plain"});
    res.end();
    return;  	
  }
  
  if(req.body.updatekey != null)
  {
  	fs.writeFile(CFG_PATH+'/.updated', req.body.updatekey,function(err){
  	
	});  	
  }
  
  fs.exists(CFG_PATH+'/.config.db',function(exists){
  if(exists)
  {
  fs.readFile(CFG_PATH+'/.config.db', { encoding: 'utf8' }, function (err, config) {

    try{
    var dString = JSON.parse(config);
    }catch(exception){
    
      fs.writeFile(CFG_PATH+'/.config.db', "{\"key\":\"" + req.body.key + "\",", function(err) {
        if (err) {

           res.writeHead(200, "OK", {
           'Content-Type': 'text/html'
          });
          res.end();
        } else {
           console.log("The file was saved!");

           res.writeHead(200, "OK", {
            'Content-Type': 'text/html'
           });
          res.end();
       }
      });
      return;
    }

    
    key = dString.key; 
    existedName = dString.name;
    if(err)//Write
    {
      fs.writeFile(CFG_PATH+'/.config.db', "{\"key\":\"" + req.body.key + "\",", function(err) {

        if (err) {

          res.writeHead(200, "OK", {
            'Content-Type': 'text/html'
          });
          res.end();
        } else {
          console.log("The file was saved!(file)");

          res.writeHead(200, "OK", {
            'Content-Type': 'text/html'
          });
          res.end();
        }
    
      });
    }
    else//No error
    {

      if(!key || (key == req.body.key))
      {
        fs.writeFile(CFG_PATH+"/.config.db", "{\"key\":\"" + req.body.key + "\",", function(err) {
      
          if (err) {
            console.log(err);
            res.writeHead(200, "OK", {
              'Content-Type': 'text/html'
            });
            res.end();
          } else {
            console.log("The file was saved!");
            res.writeHead(200, "OK", {
              'Content-Type': 'text/html'
            });
            res.end();
          }
      
        });
      }
      else
      {

          fs.writeFile(CFG_PATH+"/.config.db", "{\"key\":\"" + key + "\", \"new_key\":\"" + req.body.key + "\",", function(err) {

          if (err) {
            console.log(err);
            res.writeHead(200, "OK", {
              'Content-Type': 'text/html'
            });
            res.end();
          } else {
            console.log("The file was saved!");
            res.writeHead(200, "OK", {
              'Content-Type': 'text/html'
            });
            res.end();
          }
          });
      }
    }//No error
  });//readFile
  }
  else
  {
 console.log('Not readFile');

   fs.writeFile(CFG_PATH+"/.config.db", "{\"new_key\":\"" + req.body.key + "\",", function(err) {

     if (err) {
       console.log(err);
       res.writeHead(200, "OK", {
       'Content-Type': 'text/html'
       });
      res.end();
     } else {
       console.log("The file was saved!");
       res.writeHead(200, "OK", {
       'Content-Type': 'text/html'
       });
      res.end();
     }
    
     });
}    
  });//fs.exists
});

callback = function(response) {
  var str = ''
  response.on('data', function(chunk) {
    str += chunk;
  });

  response.on('end', function() {
  });
}
