#!/bin/sh
# showIPandMAC.sh

echo "23" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio23/direction
 
IP=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)
MAC=$(ip link show wlan0 | awk '/ether/ {print $2}')
  
echo "IP Address: $IP"
echo "MAC Address: $MAC"

if [ $IP ] ; 
then 
	echo '1' > /sys/class/gpio/gpio23/value
else
	echo '0' > /sys/class/gpio/gpio23/value
fi
   
# EndOfFile
   
