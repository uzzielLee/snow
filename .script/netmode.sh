#!/bin/sh

echo "22" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio22/direction

GPIOVALUE=$(cat /sys/class/gpio/gpio22/value)
gp="1"

MAC=$(ip link show wlan0 | awk '/ether/ {print $2}')
IP=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)

if [ $GPIOVALUE == $gp ]; then
 source $HOME/.script/run_wifi_cresprit.sh	
 check_count=0
 while [ -z "$IP_Check" -a $check_count -lt 10 ] 
 do
 sleep 1
 check_count=`expr $check_count + 1`
 IP_Check=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)
 echo "." 
 done
 if [ -z "$IP_Check" ] ; then
  echo "It doesn't get IP Address !!!"
  ifconfig wlan0 down
  kill -9 $(pidof udhcpc)
 else
  source $HOME/.script/bootapp.sh
 fi
else
 SERIAL=$(node $HOME/.script/init/init.js |awk '/serial / {print $2}' | cut -d/ -f 1)
 wait
 sed 's/SNOW_AP/'SNOW_${SERIAL}'/g' $HOME/.script/softap/rtl_hostapd.conf > /root/rtl.conf
 source $HOME/.script/run_softap_mode.sh
 source $HOME/.script/aloohUPNP_Device.sh&
 sleep 1
 IP=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1) 
 if [ -z "$IP" ] ; then
  echo "error"
 else
  check_count1=0
  while [ -z "$IP_Check" ] 
  do
  IP_Check=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)
  echo "****************************IP Address 3: $IP"
  sleep 1
  node $HOME/.script/init/fileuploader.js & 
  wait
  source $HOME/.script/run_wifi.sh
  sleep 5
  IP_Check=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)
  echo "*********************************IP Address 3: $IP_Check"
  if [ -z "$IP_Check" ] ; then
   sleep 5
   echo "*********************************changNetwork.sh again"
   source $HOME/.script/changeNetwork2AP.sh
   wait
   sleep 5
   source $HOME/.script/run_softap_mode.sh
   source $HOME/.script/aloohUPNP_Device.sh &
   sleep 1
  fi
  done
 fi
fi 
