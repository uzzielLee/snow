#!/bin/sh

echo "====================" 
echo "   Network Manger "
echo "===================="

insmod $HOME/wifi/wlan.ko 

if [ ! -d "/sys/class/gpio/gpio22" ]; then
	echo "22" > /sys/class/gpio/export
fi

echo "in" > /sys/class/gpio/gpio22/direction

GPIOVALUE=$(cat /sys/class/gpio/gpio22/value)

lan=$(cat /proc/net/wireless | grep wlan0)

case "$1" in 

'start') 
if [ -f $CFGPATH/interfaces ]; then
cp $CFGPATH/interfaces /etc/network
fi
UsingWlan=$(ifconfig | grep wlan0)
#UsingEth=$(ifconfig | grep eth0)
if [ -n "$UsingWlan" ] ; then
echo "Network Interface is already running!"
exit
fi

#if [ -n "$UsingEth" ] ; then
#echo "Network Interface is already running!"
#exit
#fi

echo " - start "

if [ -z "$lan" ] ; then
 echo " + Not found! WiFi. "
# $NPATH/netled.sh off
else

MAC=$(ip link show wlan0 | awk '/ether/ {print $2}')
IP=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)
N_TYPE=$(cat $CFGPATH/network.conf)


if [ $GPIOVALUE -eq "1" ]; then  # CLIENT MODE 
echo "Network Type : $N_TYPE"
echo " + WLAN setting Start. "
if [ "$N_TYPE" = "static" ] ; then
 source $NPATH/run_network_s.sh&
 sleep 3 
# ethOperstate=$(cat /sys/class/net/eth0/operstate)
# isUsingEth0=$(cat /etc/network/interfaces | grep eth0 | grep -v '#')
 wlan=$(cat /etc/network/interfaces | grep wpa-ssid | grep -v '#')

else
 source $NPATH/run_network.sh&	
 sleep 2
fi

 wlanIP_Check=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)
# ethIP_Check=$(ip addr show eth0 | awk '/inet / {print $2}' | cut -d/ -f 1)

echo " + WLAN Setting finished. "


#echo " + Ethernet setting Start. "
#if [ $N_TYPE = "static" ] ; then
# source $NPATH/run_network_eth_s.sh 
#else
# source $NPATH/run_network_eth.sh &
#fi
#echo " + Ethernet setting Finished. " 


else # AP MODE 

echo " + AP Mode ( for Network Setting ) "
# $NPATH/netled.sh AP
 SERIAL=$($HOME/.script/init/serial_only)
 wait
 sed 's/SNOW_AP/SN_'${SERIAL}'/g' $HOME/.script/softap/rtl_hostapd.conf > /root/rtl.conf
 source $HOME/.script/run_softap_mode.sh
 source $HOME/.script/aloohUPNP_Device.sh&
 sleep 1
 IP=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1) 
 if [ -z "$IP" ] ; then
  echo "error"
 else
  check_count1=0
  while [ -z "$IP_Check" ] 
  do
  IP_Check=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)
  echo "****************************IP Address 3: $IP"
  sleep 1
  node $HOME/.script/init/fileuploader.js & 
  wait
  source $HOME/.script/run_wifi.sh
  sleep 5
  IP_Check=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)
  echo "*********************************IP Address 3: $IP_Check"
  if [ -z "$IP_Check" ] ; then
   sleep 5
   echo "*********************************changNetwork.sh again"
   source $HOME/.script/changeNetwork2AP.sh
   wait
   sleep 5
   source $HOME/.script/run_softap_mode.sh
   source $HOME/.script/aloohUPNP_Device.sh &
   sleep 1
  fi
  done
 fi
fi 
fi


#if [ $N_TYPE = "static" ] ; then
#	ipCnt=$(grep -c address /etc/network/interfaces)
#	
#	if [ "$ipCnt" = "2" -o "$ipCnt" = "1" ] ; then
#		$NPATH/ftp_trigger.sh&
#	fi
#else
#	$NPATH/ftp_trigger.sh&
#fi


#rdate -s time.bora.net
#hwclock -w

;;

'stop')

echo " - stop "
 # killall 
 ps | grep "bftpd" | grep -v grep | awk '{print $1}' | xargs kill -9 > /dev/null 
 $NPATH/InitNet.sh 
 rmmod wlan
;;

'restart')

echo " - stop "
 pid=$(pidof bftpd)
 
if [ $pid ]; then
kill $pid  > /dev/null
fi

 $NPATH/InitNet.sh 
# $0 stop
# wait
# sleep 2 
 $0 start

 ;;

'setdhcp')
node $NPATH/setupNetworkDhcp.js
sync

#export N_TYPE="dhcp"
 ;;

'setstatic')
node $NPATH/setupNetworkStatic.js
sync

 ;;

 
*)
echo  "usage: $0 {start|stop|restart|setdhcp|setstatic}"
esac


#rdate -s time.bora.net
#hwclock -w

