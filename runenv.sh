#!/bin/sh

if [ "$HOME" = "/mnt/mmc/snow" ]; then
cp /mnt/mmc/snow/bash /bin/bash
ln -s /mnt/mmc/usr/local/gcc/bin/gcc /bin/cc
fi

hostname -F $HOME/.script/hostname

echo "##########################"
echo "This is ALOOH Init Script"
echo "##########################"

#rdate -s time.bora.net
#rdate -s time-a.nist.gov 

#export HOME=/mnt/mmc/snow
export LD_LIBRARY_PATH=/lib:/usr/local/lib:/usr/lib:/mnt/mmc/usr/local/lib:/mnt/mmc/usr/lib:/mnt/mmc/usr/local/gcc/lib
export GIT_EXEC_PATH=/mnt/mmc/snow/.git-arm/libexec/git-core
#export PATH=$PATH:/mnt/mmc/snow/.git-arm/bin:/mnt/mmc/usr/local/bin:/usr/local/bin:/mnt/mmc/local/bin
export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules:/usr/lib/node_modules:/mnt/mmc/usr/local/lib/node_modules:/root/snow/lib/node_modules
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/bin/X11:/usr/local/bin:/mnt/mmc/snow/.git-arm/bin:/mnt/mmc/usr/local/bin:/usr/local/bin
export PATH=/mnt/mmc/usr/local/gcc/bin:$PATH
export CFGPATH=/root/.config
#export SERVER_URL=52.69.220.236
export SERVER_URL=api.alooh.io
export SERVER_PORT=50001
export MQTT_URL=mqtt.alooh.io
export MQTT_PORT=1883

echo $HOME
echo $LD_LIBRARY_PATH
echo $GIT_EXEC_PAH
echo $PATH
echo "##########################"

export NPATH=$HOME/.script/network

$HOME/Manager_Network.sh start

if [ "$HOME" = "/mnt/mmc/snow" ]; then
$HOME/Manager_Developer.sh start
else  
$HOME/.script/launcher start 
fi 

