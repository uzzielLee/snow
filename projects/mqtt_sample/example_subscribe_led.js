var mqtt_sdk = require('snow/mqttsdk');
var GPIO = require('snow/gpio');

var gpio = new GPIO.gpio();
var nGpio = 2;
gpio.open();
gpio.pinMode(nGpio, 1);

 gpio.digitalWrite(nGpio, 1);
//Listener for error
mqtt_sdk.on('error', function(reason){
console.log('error : '+reason);
});

//Listener to receive the device Information
mqtt_sdk.on('deviceInfo', function(_deviceInfo){

  console.log(_deviceInfo.topicId+' '+_deviceInfo.keyInfo[0].key);
  //subscribe as based on TopicID of the device
  mqtt_sdk.AloohMessage_Subscribe(_deviceInfo.topicId, _deviceInfo.keyInfo[0].key);
  
});

//Listener for subscribe. It was called when receive a subscribe message
//_message : JSON Format
mqtt_sdk.on('message', function(_message){
  console.log('message :'+_message);
  var dStr = JSON.parse(_message);
  var msg = dStr.data.sensors[0].data_points[0].v; 
   console.log('msg :'+msg);
   
  if(msg == "on" || msg == "ON" || msg == "On")
    gpio.digitalWrite(nGpio, 1);
  
  if(msg == "off" || msg == "OFF" || msg == "Off")
    gpio.digitalWrite(nGpio, 0);
});

//Request a device information for the device(TopicID, AccessKey, AccessKey Label)
mqtt_sdk.AloohCloud_RequestDeviceInfo();
