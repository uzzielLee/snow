var mqttSdk = require('snow/mqttsdk');
var ADC = require('snow/adc');
var adc = new ADC.adc();
var adcPin = 3;

//Open a ADC interface
adc.open();
//Select a pin number on ADC interface
adc.attach(adcPin);
//Request a device information for the device(TopicID, AccessKey, AccessKey Label)
mqttSdk.AloohCloud_RequestDeviceInfo();

//Listener for error
mqttSdk.on('error', function(reason) {
  console.log('error : ' + reason);
});


//Listener to receive the device Information
mqttSdk.on('deviceInfo', function(_deviceInfo) {
  //Request sensor names registered on the device with TopicID of a selected device
  mqttSdk.AloohCloud_RequestSensorList(_deviceInfo.topicId, _deviceInfo.keyInfo[0].key);

  //Listener to receive sensor names
  mqttSdk.on('sensors', function(_sensorNames) {
    var sensorName = null;

    setInterval(function() {
      mqttSdk.AloohMessage_ResetPublishData();

      //Read a data from the pin 3 on ADC interface every 1sec
      input = adc.read(adcPin, 1000);

      voltage = (input * 3.3 / 4096);
      temperature = (voltage) * 100;

      result = temperature.toFixed(1);

      if (_sensorNames[0] == null)
        sensorName = 'default';
      else
        sensorName = _sensorNames[0];

      var time = new Date();
      //Set a data to publish(Sensor Name, Temperature Data, Time(UTC Format))
      mqttSdk.AloohMessage_SetPublishData(sensorName, result, time.toString());
      //Publish data setting with TopicID
      mqttSdk.AloohMessage_Publish(_deviceInfo.topicId, _deviceInfo.keyInfo[0].key);
    }, 1000);

  });
});