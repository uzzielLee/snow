var mqtt_sdk = require('snow/mqttsdk');

//Listener for error
mqtt_sdk.on('error', function(reason){
console.log('error : '+reason);
});

//Listener to receive the device Information
mqtt_sdk.on('deviceInfo', function(_deviceInfo){

  console.log(_deviceInfo.keyInfo[0].key+' '+_deviceInfo.topicId);
  //subscribe as based on TopicID of the device
  mqtt_sdk.AloohMessage_Subscribe(_deviceInfo.topicId, _deviceInfo.keyInfo[0].key);
  
});

//Listener for subscribe. It was called when receive a subscribe message
//_message : JSON Format
mqtt_sdk.on('message', function(_message){
  console.log('message :'+_message);
});

//Request a device information for the device(TopicID, AccessKey, AccessKey Label)
mqtt_sdk.AloohCloud_RequestDeviceInfo();
